use actix_web::{middleware, web, App, HttpRequest, HttpResponse, HttpServer, Responder};
use async_trait::async_trait;
use serde::{Deserialize, Serialize};
use std::sync::{Arc, Mutex};

#[derive(Serialize, Deserialize, Clone)]
struct Person {
    first: String,
    last: String,
}

#[actix_web::main]
async fn main() -> Result<(), Error> {
    // Create some global state prior to building the server
    //type _State = SharedMemoryState;
    type _State = SqliteState;
    let state = _State::new().await?;

    // Setup logger based on the RUST_LOG env variable. If not set, default to RUST_LOG="actix_web=info"
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("actix_web=info"))
        .init();

    HttpServer::new(move || {
        App::new()
            .data(state.clone()) // add shared state
            .wrap(middleware::Logger::default()) // Log every request
            .service(greet)
            .route("/person/{id}", web::get().to(get_person::<_State>))
            .service(
                web::resource("/person/")
                    .route(web::get().to(get_persons::<_State>))
                    .route(web::post().to(post_person::<_State>)),
            )
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await?;
    Ok(())
}

#[actix_web::get("/")]
async fn greet(_: HttpRequest) -> impl Responder {
    "Chuck Norris counted to infinity - twice!"
}

type SharedMemoryState = Arc<Mutex<MemoryState>>;

/// GET /person/{id}
async fn get_person<S: State>(state: web::Data<S>, req: HttpRequest) -> HttpResponse {
    // Here, we try to parse the param `id` and check if it is a valid index for the person list.
    // If successful: `Some(id)`. Else `None`
    let id: Option<usize> = req
        .match_info()
        .get("id")
        .and_then(|s_id: &str| s_id.parse::<usize>().ok());
    let person = if let Some(i) = id {
        state.get_person(i).await
    } else {
        Ok(None)
    };
    match person {
        Ok(Some(person)) => HttpResponse::Ok().json(person),
        Ok(None) => HttpResponse::NotFound().body("Invalid id"),
        Err(_) => HttpResponse::InternalServerError().finish(),
    }
}

/// GET /person/
async fn get_persons<S: State>(state: web::Data<S>, _: HttpRequest) -> HttpResponse {
    match state.get_persons().await {
        Ok(result) => HttpResponse::Ok().json(result),
        Err(_) => HttpResponse::InternalServerError().finish(),
    }
}

/// POST /person/
async fn post_person<S: State>(state: web::Data<S>, p: web::Json<Person>) -> HttpResponse {
    match state.post_person(p.0).await {
        Ok(()) => HttpResponse::Ok().body(""),
        Err(_) => HttpResponse::InternalServerError().finish(),
    }
}

/// Abstraction of the underlaying state (either sqlite or in memory)
#[async_trait]
trait State {
    async fn get_persons(&self) -> Result<Vec<Person>, Error>;
    async fn get_person(&self, id: usize) -> Result<Option<Person>, Error>;
    async fn post_person(&self, p: Person) -> Result<(), Error>;
}

#[derive(Clone)]
struct MemoryState {
    persons: Vec<Person>,
}

impl MemoryState {
    fn new() -> Self {
        Self {
            persons: vec![
                Person {
                    first: String::from("Chuck"),
                    last: String::from("Norris"),
                },
                Person {
                    first: String::from("Gunnery Sgt."),
                    last: String::from("Hartmann"),
                },
            ],
        }
    }
}

#[async_trait]
impl State for SharedMemoryState {
    async fn get_persons(&self) -> Result<Vec<Person>, Error> {
        Ok(self.lock().unwrap().persons.clone())
    }
    async fn get_person(&self, id: usize) -> Result<Option<Person>, Error> {
        Ok(self.lock().unwrap().persons.get(id).map(Person::clone))
    }
    async fn post_person(&self, p: Person) -> Result<(), Error> {
        self.lock().unwrap().persons.push(p);
        Ok(())
    }
}

#[derive(Clone)]
struct SqliteState {
    pool: sqlx::Pool<sqlx::Sqlite>,
}

impl SqliteState {
    async fn new() -> Result<Self, Error> {
        let pool = sqlx::sqlite::SqlitePoolOptions::new()
            .max_connections(5)
            .connect("state.db")
            .await?;
        sqlx::query!("CREATE TABLE IF NOT EXISTS persons (id INTEGER PRIMARY KEY, first TEXT NOT NULL, last TEXT NOT NULL)").execute(&pool).await?;
        Ok(Self { pool })
    }
}

#[async_trait]
impl State for SqliteState {
    async fn get_persons(&self) -> Result<Vec<Person>, Error> {
        Ok(sqlx::query_as!(Person, "SELECT first, last FROM persons")
            .fetch_all(&self.pool)
            .await?)
    }
    async fn get_person(&self, id: usize) -> Result<Option<Person>, Error> {
        let id_sqlite = id as i64;
        Ok(sqlx::query_as!(
            Person,
            "SELECT first, last FROM persons WHERE id = ?",
            id_sqlite
        )
        .fetch_optional(&self.pool)
        .await?)
    }
    async fn post_person(&self, p: Person) -> Result<(), Error> {
        sqlx::query!(
            "INSERT INTO persons (first, last) VALUES (?, ?)",
            p.first,
            p.last
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }
}

pub struct Error(pub String);

impl<T: std::fmt::Display> From<T> for Error {
    fn from(src: T) -> Error {
        Error(format!("{}", src))
    }
}
impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.pad(self.0.as_str())
    }
}
