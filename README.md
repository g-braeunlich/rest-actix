# REST demo

## Run Server

```bash
cargo run
```

## Client

```bash
curl localhost:8080/
curl localhost:8080/person/0
curl localhost:8080/person/1
curl localhost:8080/person/
curl -H "Content-Type: application/json" --data '{"first":"Mister", "last": "X"}' localhost:8080/person/
```
